Vlasta Redl - Čarodějova píseň
                                 https://youtu.be/zeo9FxM6aAA
Orig: D

I:
♪  E     A   F#  D  B  C  A

♪  E      A                 E       D         C      A
   Leží a spí tam slabounká tichá a já ji mám vzít a nést
             A5                C/D       Bmaj          C/D    Dsus2
Koberci květů  někam do jiných světů jen pryč z těchto měst a hvězd

    Emi               Hmi        C    D               Ami
Kde zas bude tančit a na zlou si hrát od smíchu uplakaná
      Emi                Hmi7        D        /C
A kde vlasy si nechá zas divoce vlát do větru co po ní
G    D   G6     C     G F Eb
vůně pak uklidí rád

Dsus2  Asus4     Hmi    Ami7    Bmaj       Ami7     D
Leží a spí tam a já nepospícham říct jí že vypršel  čas ... čas
A                     Hmi        Ami7        Gmi6     /C       D
slibů a chyb jak bych chránil ji líp kdybych věděl že potká mě zas

       Emi                Hmi     C      D                  Ami7
Teď už jen její tíhu smím na sebe brát a doufat že zláme mi vaz
         Emi             Hmi     C            D       /C
Abych se nemohl vrátit a na věky lhát že jsem nenašel nepoznal
/G          /E       C     G  F  Eb
nic víc než jen její hlas

Asus4               D                Gmi6      /C    D
Leží a spí tam a já prším a svítám a davy jdou skrze nás
Asus4               Hmi     C/D Gmi6      /C           D  
A s očima do ó celá člověčí zoo zdráhá se přijmout ten vzkaz

      Emi7             Hmi       C     D               Ami7
Že se loučí a vrací se do jiných let a do jiných koloběhů
        Emi               Hmi     C     D          Ami7    G6
A až se poslední plamínek promění v led vytrysknou gejzíry sluncí
   Hmi     Cmaj   G  F  Eb
až za onen svět

Asus4            Hmi    C       Gmi6     C9      D
Leží a spí tam a já nepospícham říct jí  vypršel čas

O:
Asus4   Hmi7 (flag)  C  Gmi6  C9  D
