# chords-and-tabs
Chords and tabs of songs I like and haven't found anywhere else in quality sufficient for my campfire playing

* Songs are supposed to work in plaintext with fixed-width fonts
* written following rules described in
  [chords.vim README](https://gitlab.com/ins-pirat-ion/sandpile#chordsvim)
  so they can be highlighted in `vim` ( http://www.vim.org/ ) by `chords.vim`
  and transposed by [chords_transpose.rb](https://gitlab.com/ins-pirat-ion/sandpile#chords_transposerb)
* find coloured / highlighed html versions of songs in [vimhighlighted](vimhighlighted)
  and PDFs in [vimhighlighted_pdf](vimhighlighted_pdf)
