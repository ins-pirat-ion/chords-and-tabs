Vítězslav Nezval, Karel Plíhal - Píseň

https://youtu.be/5-Tz2NiLxZQ

Orig: F - Kapo: 3

I:
[: D           /A       D         /A    D :]
♪  D  ^A ^F# G E D E F#  vA ^F# G E D E D

1.
D           /A         D              Emi
   Dva páry černejch koní přes krajky polštáře
Emi  /F#    /C# A    F#mi /H /C# /D  /E G  G G
   rukou si oči cloní dvě dá my  v kočá ře.
/G /F#um /E /D /E A

2.
D           /A       D          Emi
   Jedna je plavovláska a druhá bruneta
Emi  /F# /C#   A     F#mi /H /C# /D /E G  G G
   a obě trápí láska   a  je dou do světa.
   /G /F#um /E /D /E A

I:
[:4x D         /A       D         /A     D :]
♪    D ^A ^F# G E D E F#  vA ^F# G E D E D

3.
D          /A        D            Emi
   Děti si hrají s míčem, dámy se uklání
Emi  /F#  /C#    A     F#mi /H /C# /D /E G G G
   a kočí práská bičem   a  zvoní  kleká ní...
